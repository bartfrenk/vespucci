from typing import List, TypeVar
import pytest

from vespucci import Edge, Store


T = TypeVar("T")


class MockStore(Store[T]):
    def __init__(self):
        self.edges = []

    async def save(self, edge: Edge[T]) -> None:
        self.edges.append(edge)

    async def load(self, _src: T) -> List[T]:
        return []


@pytest.fixture
def store():
    return MockStore[int]()
