# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## [v0.7.0] - 2024-02-22

### Changed

- Update pydantic version to major version 2 (was 1)

## [v0.6.0] - 2021-07-13

### Added

- Log crawler and oracle exceptions
- Add sequence function to lazily sequence async functions
- Expose crawler status as a public method

## [v0.2.0] - 2021-04-14

### Changed

- Allow multiple crawlers on a single oracle.

## [v0.1.0] - 2021-04-02
