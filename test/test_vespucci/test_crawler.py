from contextlib import AsyncExitStack
from typing import TypeVar, Tuple, List

import pytest

from test_vespucci.utils import Graph, create_oracle, create_store
from vespucci import Crawler, Edge


N = TypeVar("N")


async def _run_crawler(
    graph: Graph[N], seeds: List[Tuple[N, int]]
) -> Tuple[List[Edge[N]], List[N]]:

    oracle = create_oracle(graph)
    async with AsyncExitStack() as stack:
        await oracle.value.enter(stack)
        store = await create_store().enter(stack)
        crawler = await Crawler(oracle.value, store).enter(stack)
        for (node, rank) in seeds:
            await crawler.seed(node, rank)
        await crawler.run()
    return store.edges, oracle.fn.calls


class TestCrawler:
    @pytest.mark.asyncio
    async def test_single_node_graph(self):
        actual, _ = await _run_crawler(Graph({0: []}), [(0, 1)])
        assert set(actual) == set([Edge(0, None)])

    @pytest.mark.asyncio
    async def test_single_edge_graph(self):
        actual, _ = await _run_crawler(Graph({0: [1]}), [(0, 1)])
        assert set(actual) == set([Edge(0, 1)])

    @pytest.mark.parametrize("rank", range(1, 11))
    @pytest.mark.asyncio
    async def test_cycle_graph_with_bounded_rank(self, rank):
        graph = Graph.cycle(10)
        actual, _ = await _run_crawler(graph, [(0, rank)])
        assert set(actual) == set(graph.edges[:rank])

    @pytest.mark.parametrize("size", range(2, 11))
    @pytest.mark.asyncio
    async def test_complete_graph(self, size):
        graph = Graph.complete(size)
        actual, _ = await _run_crawler(graph, [(0, 2)])
        assert set(actual) == set(graph.edges)

    @pytest.mark.asyncio
    async def test_rank_larger_than_diameter(self):
        graph = Graph.complete(2)
        actual, calls = await _run_crawler(graph, [(0, 10)])
        assert set(actual) == set(graph.edges)
        assert len(actual) == 2
        assert set(calls) == set([0, 1])

    @pytest.mark.asyncio
    async def test_avoid_calling_oracle_fn_twice(self):
        graph = Graph.complete(100)
        _, calls = await _run_crawler(graph, [(0, 100)])
        assert len(calls) == len(set(calls))
