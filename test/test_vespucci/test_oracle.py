import asyncio
from typing import List, Tuple, TypeVar
from contextlib import contextmanager

import pytest

from test_vespucci.utils import Graph, create_oracle
from vespucci import Oracle, Edge

T = TypeVar("T")

TAG = "X"


@contextmanager
def registered(oracle: Oracle, capacity: int = 0):
    oracle.register(TAG, capacity)
    yield
    oracle.unregister(TAG)


async def _seed(oracle: Oracle[T], seeds: List[Tuple[T, int]]):
    for (node, rank) in seeds:
        await oracle.seed(TAG, node, rank)


async def _reveal(oracle: Oracle[T], result: List[T]):
    it = oracle.reveal(TAG)
    try:
        while True:
            result.append(await asyncio.wait_for(it.__anext__(), 0.1))
    except asyncio.TimeoutError:
        pass


async def _reveal_all(graph, seeds):
    async with create_oracle(graph).value as oracle:
        with registered(oracle):
            await _seed(oracle, seeds)

            actual = []
            try:
                await _reveal(oracle, actual)
            except asyncio.TimeoutError:
                pass
            return set(actual)


class TestOracle:
    _TIMEOUT = 0.1

    @pytest.mark.asyncio
    async def test_single_node_graph(self):
        actual = await _reveal_all(Graph({1: []}), [(1, 1)])
        assert actual == set([(Edge(1, None), 1)])

    @pytest.mark.asyncio
    async def test_single_edge_graph(self):
        actual = await _reveal_all(Graph({1: [2]}), [(1, 1)])
        assert actual == set([(Edge(1, 2), 1)])

    @pytest.mark.asyncio
    async def test_star_graph(self):
        actual = await _reveal_all(Graph({1: [2, 3, 4]}), [(1, 1)])
        assert actual == set([(Edge(1, 2), 1), (Edge(1, 3), 1), (Edge(1, 4), 1)])

    @pytest.mark.asyncio
    async def test_multiple_seeds(self):
        actual = await _reveal_all(Graph({1: [2], 2: [3], 3: [1]}), [(1, 1), (2, 1), (3, 1)])
        assert actual == set([(Edge(1, 2), 1), (Edge(2, 3), 1), (Edge(3, 1), 1)])

    @pytest.mark.asyncio
    async def test_initial_in_process_count_is_zero(self):
        async with create_oracle(Graph({1: []})).value as oracle:
            with registered(oracle):
                assert oracle.in_process(TAG) == 0

    @pytest.mark.asyncio
    async def test_seeding_increases_in_process_count(self):
        count = 0
        async with create_oracle(Graph({1: []})).value as oracle:
            with registered(oracle):
                assert oracle.in_process(TAG) == count
                for (node, rank) in [(1, 1), (2, 1), (3, 1)]:
                    await oracle.seed(TAG, node, rank)
                    count += 1
                    assert oracle.in_process(TAG) == count

    @pytest.mark.asyncio
    async def test_revealing_decreases_in_process_count(self):
        async with create_oracle(Graph({1: [], 2: [], 3: []})).value as oracle:
            with registered(oracle):
                for (node, rank) in [(1, 1), (2, 1), (3, 1)]:
                    await oracle.seed(TAG, node, rank)
                count = 3
                assert oracle.in_process(TAG) == count

                it = oracle.reveal(TAG)
                while count > 0:
                    _ = await asyncio.wait_for(it.__anext__(), self._TIMEOUT)
                    count -= 1
                    assert oracle.in_process(TAG) == count
