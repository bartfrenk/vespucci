from __future__ import annotations

import asyncio
from collections import namedtuple
from typing import Generic, List, Mapping, TypeVar

from vespucci import Edge, Oracle, Store

N = TypeVar("N")


class Graph(Generic[N]):
    def __init__(self, neighbors: Mapping[N, List[N]]):
        self.neighbors = neighbors

    @property
    def edges(self) -> List[Edge[N]]:
        def it():
            for src in self.neighbors:
                for dst in self.neighbors[src]:
                    yield Edge(src, dst)

        return list(it())

    @classmethod
    def cycle(cls, size: int) -> Graph[int]:
        edges = {}
        for i in range(size):
            edges[i] = [(i + 1) % size]
        return cls(edges)  # type: ignore

    @classmethod
    def complete(cls, size: int) -> Graph[int]:
        edges = {}
        for i in range(size):
            edges[i] = [j for j in range(size) if j != i]
        return cls(edges)  # type: ignore


class MockStore(Store[N]):
    def __init__(self):
        self.edges = []

    async def save(self, edge: Edge[N]) -> None:
        self.edges.append(edge)

    async def load(self, _src: N) -> List[N]:
        return []


def create_store() -> MockStore[N]:
    return MockStore()


def create_oracle(graph: Graph[N]) -> Oracle[N]:
    class Fn:
        def __init__(self):
            self.calls: List[N] = []

        async def __call__(self, src: N):
            self.calls.append(src)
            try:
                result = []
                for dst in graph.neighbors[src]:
                    result.append(Edge(src, dst))
                return result
            except asyncio.CancelledError:
                pass

    fn = Fn()
    return namedtuple("Result", ["value", "fn"])(Oracle(fn), fn)  # type: ignore
