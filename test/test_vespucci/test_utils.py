import asyncio
from typing import Iterator, Tuple, TypeVar
from itertools import product

import pytest

from vespucci import merge, sequence


X = TypeVar("X")


async def _produce(queue: asyncio.Queue, items: Iterator[Tuple[float, X]]):
    for (delay, x) in items:  # pylint: disable=redefined-argument-from-local
        await asyncio.sleep(delay)
        await queue.put(x)
    return None


class TestMerge:
    async def _merge(self, producers):
        inqs = {tag: asyncio.Queue() for tag in producers}
        for (tag, items) in producers.items():
            asyncio.create_task(_produce(inqs[tag], iter(items)))
        merged = await merge(inqs)
        actual = []
        while True:
            try:
                actual.append(await asyncio.wait_for(merged.get(), timeout=0.2))
            except asyncio.TimeoutError:
                merged.stop()
                break
        return actual

    @pytest.mark.asyncio
    async def test_merge_single_producer(self):
        producers = {"A": [(0, 1)]}
        actual = await self._merge(producers)
        assert actual == [("A", 1)]

    @pytest.mark.asyncio
    async def test_merge_two_producers(self):
        producers = {"A": [(0, 1), (0, 2)], "B": [(0, 3), (0, 4)]}
        actual = await self._merge(producers)
        assert set(actual) == set([("A", 1), ("A", 2), ("B", 3), ("B", 4)])

    @pytest.mark.asyncio
    async def test_interleave_two_producers(self):
        producers = {"A": [(0, 1), (0.2, 3)], "B": [(0.1, 2), (0.1, 4)]}
        actual = await self._merge(producers)
        assert actual == [("A", 1), ("B", 2), ("A", 3), ("B", 4)]

    @pytest.mark.asyncio
    async def test_with_more_and_more_active_producers(self):
        tags = range(20)
        producers = {tag: [(0, i) for i in range(100)] for tag in tags}
        actual = await self._merge(producers)
        assert set(actual) == set(product(tags, range(100)))


class TestSequence:
    def func(self, i):
        async def fn(j):
            if j == i:
                return [i]
            return []

        return fn

    @pytest.mark.asyncio
    async def test_return_empty_list_on_empty_list(self):
        assert await sequence()(None) == []

    @pytest.mark.asyncio
    async def test_return_first_non_empty_list(self):
        assert await sequence(self.func(0), self.func(1), self.func(2), self.func(3))(2) == [2]

    @pytest.mark.asyncio
    async def test_return_empty_list_when_all_return_empty_list(self):
        assert await sequence(self.func(0), self.func(0), self.func(0), self.func(0))(1) == []
